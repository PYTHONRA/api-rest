## Application Node Js et React Express

## Définition :

**export default**:  
Un fichier peut avoir plusieurs `export`mais il ne peut qui avoir un seul `export default` ou zéro par fichier.
Ce que j'exporte dans ce fichier par default est le `mainRouter`.
Un export par défaut, nous pouvons le renommer comme nous le souhaitons.
Un export normal

## Les libraies utilisés

Comprendre le moteur V8 de NodeJS :
Lien Nodejs (https://nodejs.org/en/)
Lien V8 (https://v8.dev/)
Node.js® est un environnement d'exécution JavaScript basé sur le moteur JavaScript V8 de Chrome .

## Création d'un nouveau dossier `src`

Se positionner sur le dossier `cd src`

Projet en Front dit `index.html`
_ Le livrable fait en Front va être lu par le navigateur.
_ Ce qui est lu par le navigateur est `index.html` qui ensuite appelle `js`, qui appelle aussi du `css`.

## Les commandes utilisées :

installation de `npm init`
installation de `npm install --save-dev typescript ts-node @types/node nodemon` typescript avec les types pour node puis nodemon
installation du dossier `dist` `npx tsc` c'est typescript compileur a chaque modification
pour tester son code `node ./dist/index.js`
installation de `npm install --save-dev parcel typescript @types/node`
installation de `npm install --save express`
installation de `npm install --save-dev @types/express`
installation des types pour express `npm i --save-dev @types/express`
installation de ` npm install --save-dev dotenv`
installation de `npm install --save-dev axios`
installation de `npm install --save-dev @types/node`
installation de `npm i --save-dev @types/cors` pour télécharger les types
installation de `npm add -D cors` ajout de la librairie
installation de `npm install --save-dev prisma`
suivre la doc `npx prisma init` on prends le CLI de prisma car on install pas prisma en globale
pour migrate prisma `npx prisma migrate dev --name init`
variable pour connecter notre mysql avec notre .env`DATABASE_URL="mysql://root:root@localhost:3306/ffcc_api?schema=public" npx prisma migrate dev --name init`
installation React `npm install react`
installation React DOM `npm install react-dom`
installation React types `npm add -D @types/react @types/react-dom`
installation de `npm install argon2`

## Les commandes pour lancer le projet :

`npm run build`Pour : _"npx tsc",_
`npm run watch` Pour: _"npx tsc -w",_
`npm run start`Pour : _"node ./dist/index.js",_
`npm run dev` Pour : _"nodemon ./dist/index.js",_
` npm db:migrate`
`db:visualize`
`npx prisma studio`

# Travailler en multi-couche

(fichier de config.ts se trouve sur le repo de Quentin)

C'est une API REST qui va permettre de communiqué avec plusieurs application ou site web.
en savoir plus ici : https://www.redhat.com/fr/topics/api/what-is-a-rest-api

parcel : un outils qui facilitent la vie des développeurs. L'essence du travail des bundlers est qu'ils prennent le code JavaScript contenu dans de nombreux fichiers et le regroupent dans un ou plusieurs fichiers, en les commandant et en les préparant au travail dans les navigateurs d'une certaine manière. De plus, grâce à divers plug-ins (plug-ins) et chargeurs, le code peut être réduit, et d'autres ressources (telles que des fichiers CSS et des images) peuvent être packagées, en plus du code. Les bundlers vous permettent d'utiliser des préprocesseurs; ils peuvent diviser le code en fragments qui se chargent lorsqu'ils deviennent nécessaires. Mais leurs capacités ne se limitent pas à cela. En fait, nous parlons du fait qu'ils aident à organiser le processus de développement.

le toggle en javascript : La méthode toggle() (bascule) de l'interface DOMTokenList supprime une marque (token) donnée de la liste et renvoie false (faux). Si token n'existe pas, il est ajouté et la fonction renvoie true.

import { createServer } from "./server";
import { PORT, NODE_ENV } from "./config";

console.log(process.env.APP_BASE_URL);

const main = async () => {
  const server = createServer();

  server.listen(PORT, () => {
    console.log(`Server tourne sur le port ${PORT} en ${NODE_ENV} mode`);
  });
};

main();
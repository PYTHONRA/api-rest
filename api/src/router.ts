import { Router, Request, Response } from "express";
import { APP_BASE_URL } from "./config";
import { userRouter } from "./module/user/userRouter";
import { prisma } from "./database";

const mainRouter: Router = Router();

console.log(APP_BASE_URL);

mainRouter.get("/", (_req: Request, res: Response) => {
  res.send("j'ai fais un call API voici la racine de mon API");
});

mainRouter.get("/villes", async (_: Request, res: Response) => {
  let ville = await prisma.villes.findMany();
  res.send('Bienvenue à : ' + ville[0].Nom_Ville);
});

mainRouter.use("/users", userRouter);

export { mainRouter };

//Le point d'entrée

import React from "react";
import ReactDOM from "react-dom";
import App from "./app";

const MOUNT_NODE = document.getElementById("app");
console.log(MOUNT_NODE);

ReactDOM.render(<App />, MOUNT_NODE, () => {
  console.log("l'application fonctionne");
});

// une instance d'axios pour faire des CALL d'API

import axios from "axios";
import { API_BASE_URL } from "./constants";
let instance = axios.create({
  baseURL: API_BASE_URL,
});

export default instance;

import dotenv from "dotenv";
import path from "path";

const envPath = path.join(__dirname, "./../../../");
dotenv.config({ path: envPath + "/.env" });

export const NODE_ENV = process.env.NODE_ENV;
export const API_BASE_URL =
  process.env.API_BASE_URL || "http://localhost:3001/api/v3/";
